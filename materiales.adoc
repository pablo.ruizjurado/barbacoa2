== Lista de materiales

// Materiales necesarios por categoría, ordenados alfabéticamente

=== Cocina y comida

* Cucharas
* Tenedores
* Platos
* Vasos

=== Mobiliario

* barbacoa (indispensable)
* Mesas
* Sillas


=== Diversión

* Radiocassette
* Altavoz
